﻿namespace _25_Warcaby_plansza
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Obrazek = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbRzedy = new System.Windows.Forms.TextBox();
            this.tbKolumny = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonGeneruj = new System.Windows.Forms.Button();
            this.labelKomunikaty = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Obrazek)).BeginInit();
            this.SuspendLayout();
            // 
            // Obrazek
            // 
            this.Obrazek.Location = new System.Drawing.Point(12, 12);
            this.Obrazek.Name = "Obrazek";
            this.Obrazek.Size = new System.Drawing.Size(500, 500);
            this.Obrazek.TabIndex = 0;
            this.Obrazek.TabStop = false;
            this.Obrazek.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Obrazek_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(522, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ilość rzędów (3-20)";
            // 
            // tbRzedy
            // 
            this.tbRzedy.Location = new System.Drawing.Point(674, 12);
            this.tbRzedy.Name = "tbRzedy";
            this.tbRzedy.Size = new System.Drawing.Size(64, 20);
            this.tbRzedy.TabIndex = 2;
            // 
            // tbKolumny
            // 
            this.tbKolumny.Location = new System.Drawing.Point(674, 38);
            this.tbKolumny.Name = "tbKolumny";
            this.tbKolumny.Size = new System.Drawing.Size(64, 20);
            this.tbKolumny.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(522, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ilość kolumn (3-20)";
            // 
            // buttonGeneruj
            // 
            this.buttonGeneruj.Location = new System.Drawing.Point(663, 64);
            this.buttonGeneruj.Name = "buttonGeneruj";
            this.buttonGeneruj.Size = new System.Drawing.Size(75, 23);
            this.buttonGeneruj.TabIndex = 7;
            this.buttonGeneruj.Text = "Generuj";
            this.buttonGeneruj.UseVisualStyleBackColor = true;
            this.buttonGeneruj.Click += new System.EventHandler(this.buttonGeneruj_Click);
            // 
            // labelKomunikaty
            // 
            this.labelKomunikaty.AutoSize = true;
            this.labelKomunikaty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelKomunikaty.Location = new System.Drawing.Point(525, 110);
            this.labelKomunikaty.Name = "labelKomunikaty";
            this.labelKomunikaty.Size = new System.Drawing.Size(0, 16);
            this.labelKomunikaty.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(748, 531);
            this.Controls.Add(this.labelKomunikaty);
            this.Controls.Add(this.buttonGeneruj);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbKolumny);
            this.Controls.Add(this.tbRzedy);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Obrazek);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Form1";
            this.Text = "Warcaby";
            ((System.ComponentModel.ISupportInitialize)(this.Obrazek)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Obrazek;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbRzedy;
        private System.Windows.Forms.TextBox tbKolumny;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonGeneruj;
        private System.Windows.Forms.Label labelKomunikaty;
    }
}

