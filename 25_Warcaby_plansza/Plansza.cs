﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _25_Warcaby_plansza
{
    class Plansza
    {
        private Graphics g;
        private int rzedy, wysPola;
        private int kolumny, szerPola;

        private List<Point> pionyCzerwone;
        private List<Point> pionyNiebieskie;
        private Point uzyty;
        private bool czyCzerwony;

        private enum etapy { zaznacz, puste }
        private etapy etap;
        private System.Windows.Forms.PictureBox obrazek;

        private string komunikat;

        public string Komunikat
        {
            get { return komunikat; }
        }
        private Color komunikatKolor;

        public Color KomunikatKolor
        {
            get { return komunikatKolor; }
        }

        public Plansza(int rzedy, int kolumny, System.Windows.Forms.PictureBox obrazek)
        {
            this.rzedy = rzedy;
            this.kolumny = kolumny;
            this.obrazek = obrazek;
            g = Graphics.FromImage(obrazek.Image);

            wysPola = obrazek.Height / rzedy;
            szerPola = obrazek.Width / kolumny;

            pionyCzerwone = new List<Point>();
            pionyNiebieskie = new List<Point>();

            for (int i = 0; i < kolumny; i++)
            {
                pionyCzerwone.Add(new Point(i, 0));
                pionyNiebieskie.Add(new Point(i, rzedy - 1));
            }
            komunikat = "Teraz Czerwony!";
            komunikatKolor = Color.Red;
            czyCzerwony = true;
            etap = etapy.zaznacz;

            Rysuj();
        }


        internal void Klikniecie(Point punkt)
        {
            int x = 0, y = 0;
            try
            {
                x = punkt.X / szerPola;
                y = punkt.Y / wysPola;
            }
            catch
            {
                MessageBox.Show("Przed grą określ parametry planszy");
                return;
            }

            List<Point> piony;
            if (czyCzerwony)
            {
                piony = pionyCzerwone;
            }
            else
            {
                piony = pionyNiebieskie;
            }


            if (etap == etapy.zaznacz)
            {
                foreach (Point p in piony)
                {
                    if (p.X == x && p.Y == y)
                    {
                        g.DrawEllipse(new Pen(Color.Black, 10), x * szerPola + szerPola / 5, y * wysPola + wysPola / 5, szerPola * 3 / 5, wysPola * 3 / 5);
                        uzyty = new Point(x, y);
                        etap = etapy.puste;
                        piony.Remove(p);
                        obrazek.Refresh();
                        return;
                    }
                }
                komunikat += "\nKliknij na odpowiedni pionek!";
            }
            else
            {
                bool puste = true;
                if (uzyty.X == x && uzyty.Y == y)
                {
                    puste = false;
                }
                foreach (Point p in pionyCzerwone)
                {
                    if (p.X == x && p.Y == y)
                    {
                        puste = false;
                    }
                }
                foreach (Point p in pionyNiebieskie)
                {
                    if (p.X == x && p.Y == y)
                    {
                        puste = false;
                    }
                }
                if (puste)
                {
                    piony.Add(new Point(x, y));
                    Rysuj();
                    czyCzerwony = !czyCzerwony;
                    etap = etapy.zaznacz;
                    if (czyCzerwony)
                    {
                        komunikat = "Teraz Czerwony!";
                        komunikatKolor = Color.Red;
                    }
                    else
                    {
                        komunikat = "Teraz Niebieski!";
                        komunikatKolor = Color.Blue;
                    }
                }
                else
                {
                    komunikat += "\nKliknij na puste pole!";
                }
            }
        }

        private void Rysuj()
        {
            g.Clear(Color.White);
            for (int i = 0; i < kolumny; i++)
            {
                for (int j = 0; j < rzedy; j++)
                {
                    if ((i + j) % 2 == 0)
                    {
                        g.FillRectangle(new SolidBrush(Color.Gray), i * szerPola, j * wysPola, szerPola, wysPola);
                    }
                    else
                    {
                        g.FillRectangle(new SolidBrush(Color.White), i * szerPola, j * wysPola, szerPola, wysPola);
                    }
                }
            }
            foreach (Point p in pionyCzerwone)
            {
                g.FillEllipse(new SolidBrush(Color.Red), p.X * szerPola + szerPola / 5, p.Y * wysPola + wysPola / 5, szerPola * 3 / 5, wysPola * 3 / 5);
            }
            foreach (Point p in pionyNiebieskie)
            {
                g.FillEllipse(new SolidBrush(Color.Blue), p.X * szerPola + szerPola / 5, p.Y * wysPola + wysPola / 5, szerPola * 3 / 5, wysPola * 3 / 5);
            }
            obrazek.Refresh();
        }
    }
}
