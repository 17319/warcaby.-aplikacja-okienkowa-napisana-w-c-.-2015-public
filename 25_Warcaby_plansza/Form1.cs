﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _25_Warcaby_plansza
{
    public partial class Form1 : Form
    {
        Plansza p;
        
        public Form1()
        {
            InitializeComponent();
            Obrazek.Image = new Bitmap(Obrazek.Width, Obrazek.Height);
        }

        private void buttonGeneruj_Click(object sender, EventArgs e)
        {
            int rzedy, kolumny;
            try
            {
                rzedy = Convert.ToInt16(tbRzedy.Text);
                if (rzedy < 3 || rzedy > 20)
                    throw new Exception();
            }
            catch
            {
                MessageBox.Show("Podaj prawidłową liczbę wierszy! (3-20)");
                return;
            }
            try
            {
                kolumny = Convert.ToInt16(tbKolumny.Text);
                if (kolumny < 3 || kolumny > 20)
                    throw new Exception();
            }
            catch
            {
                MessageBox.Show("Podaj prawidłową liczbę kolumn! (3-20)");
                return;
            }
            p = new Plansza(rzedy, kolumny, Obrazek);
            wyswietlKomunikat();
        }

        private void Obrazek_MouseClick(object sender, MouseEventArgs e)
        {
            p.Klikniecie(e.Location);
            wyswietlKomunikat();
        }

        private void wyswietlKomunikat()
        {
            labelKomunikaty.Text = p.Komunikat;
            labelKomunikaty.ForeColor = p.KomunikatKolor;
        }
    }
}
